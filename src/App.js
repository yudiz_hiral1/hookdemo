// import logo from './logo.svg'
import React from 'react'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import './App.css'
import Dashboard from './components/dashboard'
import Registration from './components/form'
import Login from './components/login'

function App () {
  return (
    <div className="App">
      <Router>
        <Routes>
          <Route path='/' element={<Registration/>} />
          <Route path='/login' element={<Login/>} />
          <Route path='/dashboard' element={<Dashboard/>} />
        </Routes>
     </Router>
    </div>
  )
}

export default App
