import React from 'react'
import { useNavigate } from 'react-router-dom'

export default function Dashboard () {
  const navigate = useNavigate()

  const logout = () => {
    sessionStorage.removeItem('LoginInfo')

    navigate('/login')
  }

  return (
    <div>
      <h1> Hello Dashboard </h1>
      <button onClick={logout}> logout </button>
    </div>
  )
}
