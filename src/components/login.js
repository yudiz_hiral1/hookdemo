import React, { useState } from 'react'
import '../styles/reg.css'
import { useNavigate } from 'react-router-dom'

export default function Login () {
  const navigate = useNavigate()
  const [loginform, setLoginForm] = useState({
    email: '', pwd: ''
  })

  //   const User = JSON.parse(localStorage.getItem('userinfo'))

  const handleInput = (event) => {
    const name = event.target.name
    const value = event.target.value
    setLoginForm({
      ...loginform, [name]: value
    })
  }

  const handleLogin = (event) => {
    event.preventDefault()
    const User = JSON.parse(localStorage.getItem('userinfo'))
    const bool = true
    for (let i = 0; i < User.length; i++) {
      if (User[i].email === loginform.email && User[i].pwd === loginform.pwd) {
        alert('You are Loged in')
        sessionStorage.setItem('LoginInfo', JSON.stringify(User[i]))
        navigate('/dashboard')
        break
      }
    }
    if (bool === false) {
      alert('please enter valid email and password')
    }
  }

  return (
        <div>
<div className='container'>
                   <div className="title">Login</div>
                   <br />
                   <form id="login_form" method="" action="#" name="regi_form">
                       <div className="user-details">
                           <div className="input-box">
                               <span className="details">Email</span>
                               <input type="text" id="email" name="email" placeholder="Enter Email" value={loginform.email} onChange={handleInput} required />
                           </div>

                           <div className="input-box">
                               <span className="details">Password</span>
                               <input type="password" id="pwd" name="pwd" placeholder="Enter password" value={loginform.pwd} onChange={handleInput} required/>
                           </div>

                       </div>
                       <div className="button">
                           <input type="submit" value="Login" className="btn" onClick={handleLogin}/>
                       </div>

                   </form>
               </div>
        </div>
  )
}
