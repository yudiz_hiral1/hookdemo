import React, { useEffect, useState } from 'react'
import '../styles/reg.css'

export default function Registration () {
  const [user, setUser] = useState({
    fullname: '', email: '', pwd: '', city: '', mobileno: '', dob: ''
  })

  const [storedata, setStoreData] = useState([])

  useEffect(() => {
    const data = JSON.parse(localStorage.getItem('userinfo'))
    if (data) {
      setStoreData(data)
    }
  }, [])

  const handleInput = (event) => {
    // console.log(event)
    const name = event.target.name
    const value = event.target.value
    setUser({
      ...user, [name]: value
    })
  }

  const handleSubmit = (event) => {
    event.preventDefault()
    setStoreData(prevFormValue => [...prevFormValue, user])
    localStorage.setItem('userinfo', JSON.stringify([...storedata, user]))
    setUser({
      fullname: '', email: '', pwd: '', city: '', mobileno: '', dob: ''
    })
  }

  // useEffect(() => {
  //   localStorage.setItem('userinfo', JSON.stringify(storedata))
  // })

  return (
    <div>
        <div className='container'>
        <div className="title">Registration</div>
        <br />
        <form id="login_form1" method="" action="#" name="regi_form">
          <div className="user-details">

            <div className="input-box">
              <span className="details">Name</span>
              <input type="text" id="fullname" name="fullname" placeholder="Enter Name" value={user.fullname} onChange={handleInput} required/>
            </div>

            <div className="input-box">
              <span className="details">Email</span>
              <input type="text" id="email1" name="email" placeholder="Enter Email" value={user.email} onChange={handleInput} required/>
            </div>

            <div className="input-box">
              <span className="details">Password</span>
              <input type="password" id="pwd1" name="pwd" placeholder="Enter password" value={user.pwd} onChange={handleInput} required/>
            </div>

            <div className="input-box">
              <span className="details">City</span>
              <input type="text" id="city" name="city" placeholder="Enter your city" value={user.city} onChange={handleInput} required/>
            </div>

            <div className="input-box">
              <span className="details">Mobile Number</span>
              <input type="tel" id="mobileno" name="mobileno" placeholder="Enter Mobile Number" value={user.mobileno} onChange={handleInput} required/>
            </div>

            <div className="input-box">
              <span className="details">Date of Birth</span>
              <input type="date" id="dob" name="dob" placeholder="Enter Date of Birth" value={user.dob} onChange={handleInput} required/>
            </div>
          </div>

          <div className="button">
            <input type="submit" value="Register" className="btn" onClick={handleSubmit}/>
          </div>

        </form>
      </div>
    </div>
  )
}
